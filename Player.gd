extends Area2D
signal hit

@export var speed = 400	# How fast the player moves (pixels/sec)
var screen_size			# Size of game window
var focus = false		# Boolean for focused mode state

func _ready():
	screen_size = get_viewport_rect().size
	hide()
	
func _process(delta):
	var velocity = Vector2.ZERO	# The player's movement vector
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	
	# Addition: Focus mode like in Touhou
	if Input.is_action_pressed("focus"):
		focus = true
	if Input.is_action_just_released("focus"):
		focus = false
	if focus == true:
		speed = 250
		$AnimatedSprite2D.set_speed_scale(0.6)
		# TODO: Add hitbox approximation graphic
	elif focus == false:
		speed = 400
		$AnimatedSprite2D.set_speed_scale(1)

	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		$AnimatedSprite2D.play()	# $ is short for get_node("GreenPart")
	else:
		$AnimatedSprite2D.stop()

	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)	# Clamp is basically a combined MIN and MAX
	position.y = clamp(position.y, 0, screen_size.y)

	if velocity.x != 0:
		$AnimatedSprite2D.animation = "walk"
		$AnimatedSprite2D.flip_v = false
		$AnimatedSprite2D.flip_h = velocity.x < 0
	elif velocity.y != 0:
		$AnimatedSprite2D.animation = "up"
		$AnimatedSprite2D.flip_v = velocity.y > 0

func _on_Player_body_entered(body):
	hide()	# Player disappears after hit
	emit_signal("hit")
	# Must be deferred as we can't change physics properties on a physics callback
	$CollisionShape2D.set_deferred("disabled", true)

func start(pos):
	position = pos
	show()
	$CollisionShape2D.disabled = false
