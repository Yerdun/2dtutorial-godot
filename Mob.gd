extends RigidBody2D

@export var min_speed = 600	# Minimum speed range
@export var max_speed = 850	# Maximum speed range
# Min and max speed ranges have been changed from the tutorial.

func _ready():
	$AnimatedSprite2D.playing = true
	var mob_types = $AnimatedSprite2D.frames.get_animation_names()
	$AnimatedSprite2D.animation = mob_types[randi() % mob_types.size()]

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
