extends Node

@export var mob_scene: PackedScene = preload("res://Mob.tscn")
# Above line is bug workaround: https://github.com/godotengine/godot/issues/45697
var score

func _ready():
	randomize()

func game_over():
	$ScoreTimer.stop()
	$MobTimer.stop()
	$Music.stop()
	$DeathSound.play()	# The original DeathSound has been changed to DeathSong. This is a certain other death sound you may be familiar with.
	
	# My addition: Differing game over messages based on score
	if score == 0:
		$HUD.show_game_over("Wow! You're\nreally bad\nat this!")
	elif score >= 15:
		$HUD.show_game_over("Hey, you're\npretty good!")
	else:
		$HUD.show_game_over("Game over!")
	
func new_game():
	score = 0
	$Player.start($StartPosition.position)
	$StartTimer.start()
	$HUD.update_score(score)
	$HUD.show_message("Get ready!")
	get_tree().call_group("mobs", "queue_free")
	$Music.play()

func _on_StartTimer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()

func _on_ScoreTimer_timeout():
	score += 1
	$HUD.update_score(score)

func _on_MobTimer_timeout():
	# Choose a random location on Path2D
	var mob_spawn_location = $MobPath/MobSpawnLocation
	mob_spawn_location.offset = randi()
	
	# Create a Mob instance and add it to the scene
	var mob = mob_scene.instance()
	add_child(mob)
	
	# Set mob's direction perpendicular to path direction
	var direction = mob_spawn_location.rotation + deg2rad(90)	# Godot uses radians, use PI or TAU (2pi) and multiply by desired value if preferred
	
	# Set mob's position to random location
	mob.position = mob_spawn_location.position
	
	# Add randomness to direction
	direction += randf_range(deg2rad(-90), deg2rad(90))
	mob.rotation = direction
	
	# Choose the velocity
	var velocity = Vector2(randf_range(mob.min_speed, mob.max_speed), 0)
	mob.linear_velocity = velocity.rotated(direction)
