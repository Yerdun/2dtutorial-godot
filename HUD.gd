extends CanvasLayer

signal start_game

func show_message(text):
	$Message.text = text
	$Message.show()
	$MessageTimer.start()

# Implementation of game over differs due to what I think are engine bugs.
func show_game_over(gameover_message):
	#show_message("Game over!")
	# Wait until MessageTimer counts down
	#await $MessageTimer.timeout # This should work, but seems bugged?
	
	$Message.text = gameover_message
	$Message.show()
	#Make a one-shot timer and wait for it to finish
	#await get_tree().create_timer(1).timeout
	$StartButton.text = "Try again?"
	$StartButton.show()

func update_score(score):
	$ScoreLabel.text = str(score)

func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")

func _on_MessageTimer_timeout():
	$Message.hide()
