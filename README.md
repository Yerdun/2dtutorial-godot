# 2dtutorial-godot

This is my take on the 2D tutorial for the Godot engine. This was made on the latest version (4.0.dev), as of git commit b060ca680.

Some things were changed around, either to work around changes in GDScript since the tutorial was written, to add features of my own, or to work around bugs that are currently in the engine. I have tried to mark those changes where I can.

You can view the tutorial here: https://docs.godotengine.org/en/latest/getting_started/first_2d_game/index.html
